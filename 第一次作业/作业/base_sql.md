

### base解析

```js
class base{
    constructor(request,res){
        this.request = request;
        this.res = res;
    }
    // 把数据通过方法的形式独立出来，有点水文字
    // 最近没什么动力看代码，没感觉
    // 老话， ctrl + 鼠标左键查看相应信息
    //获取POST数据
    GetPostData(callBack){
        let allData = '';
        //数据到来事件处理
        this.request.req.on('data',(data)=>{
            allData+=data;
        });
        //数据发送结束事件处理
        this.request.req.on('end',()=>{
            callBack(allData);
        });  
    }
```

### mysql解析

```js
// 引入一个 mysql 控件
const mysql = require('mysql');
const config = require('../config/config')


class mysqlDb{
    connect(){
        //创建连接
        const conn = mysql.createConnection({
            // config 下的数据信息，我这里随意发挥写的 config.dataHost, 比较喜欢这种写法
            host:config.db_host,
            port:config.db_port,
            user:config.db_user,
            password:config.db_password,
            database:config.db_database
        })
        //执行连接
        // 连接正相关 相关错误逻辑
        conn.connect((err)=>{
            if(err){
                console.log(err.message)
            }else{
                console.log('数据库连接成功');
            }
        })
       return conn;
    }



    //查询
    // 查询的方法，自定义的
    select(sql,callBack){
        //执行连接
        // 连接的再调用，把自定义方法调用进来，完成接下来的逻辑操作
        const conn = this.connect();
        //业务
        // 查询语句
        conn.query(sql,(err,result)=>{
            // 动态将结果返回
            callBack(err,result);
        });
        //关闭连接
        conn.end();
    }
    //增
    add(sql,callBack){
        //执行连接
        const conn = this.connect();
        //业务  1.err 2.影响行数 3.最后ID
        // 没听课，不知道要不要加判断，既然写了这么多，又是增加数据，我想还是要的
        conn.query(sql,(err,result)=>{
            callBack(err,result);
        });
        //关闭连接
        conn.end();
    }
    //改
    update(sql,callBack){
        //执行连接
        const conn = this.connect();
        //业务 1.err 2.更新行数 
        conn.query(sql,(err,result)=>{
            console.log(result)
            if(err){
                callBack(false);
            }else{
                // 改变数据的行数 change 加 ed 很好理解，已经改变的行数，already adv.的意思，中文一般会加已经，去耦合，使语法的表达形式自由度更高，这里也用的很恰当
               if(result.changedRows<=0){
                callBack(false);
               } else{
                callBack(true);
               }
            }
        });
        //关闭连接
        conn.end();        
    }
    //删除
    delete(sql,callBack){
        //执行连接
        const conn = this.connect();
        //业务 1.err 2.影响行数 
        conn.query(sql,(err,result)=>{
            // 大错误直接退场
            if(err){
                callBack(false);
            }else{
                // 找到相关的影响行数，你没影响？说明你并没有做删除的相关操作，返回失败数据
                if(result.affectedRows<=0){
                	callBack(false);
                    // 反其道而行之呢？就是给个true喽，正着写反着写随你，代码就这么多
                } else{
                	callBack(true);
                }
            }
        });
        //关闭连接
        conn.end(); 
    }
}
```