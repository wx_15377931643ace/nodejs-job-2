## index文件处理

### 新增处理

```javascript
//加载mysqlDb文件
const mysqlDb = require('../../vendor/mysqlDb');
//加载model文件
const bankCardModel = require(config.Model_Path+'/bankCard');
test(){
		//直接调用mysqlDb文件
        // const dbObj = new mysqlDb();
        // let sql = 'delete from bankCard where CardId=20';
        // dbObj.delete(sql,(status)=>{
        //     if(status){
        //         this.res.end('执行成功');
        //     }else{
        //         this.res.end('执行失败');
        //     }
        // })

		//调用model里的文件
        const model = new bankCardModel();
        let CardNo = '622509823429057';
        let AccountId = 3;
        let CardPwd = '78787';
        let CardMoney = 90;
        let CardState = 0;
        let CardTime = parseInt(Date.now()/1000);
        
        //标准的SQL代码
        // let sql = `insert into bankCard(CardNo,AccountId,CardPwd,CardMoney,CardState,CardTime)
        // values(${CardNo},${AccountId},${CardPwd},${CardMoney},${CardState},${CardTime})`;
        
        //处理后的SQL代码
        // let fields = `CardNo,AccountId,CardPwd,CardMoney,CardState,CardTime`;
        // let values = `${CardNo},${AccountId},${CardPwd},${CardMoney},${CardState},${CardTime}`;
        
        //最终处理的SQL代码
       	//主键做为字段名，数据做为赋值。
        let data = {
            CardNo:CardNo,
            AccountId:AccountId,
            CardPwd:CardPwd,
            CardMoney:CardMoney,
            CardState:CardState,
            CardTime:CardTime
        };
       
        model.add(data,(lastId)=>{
            if(lastId){
                this.res.end('执行成功');
            }else{
                this.res.end('执行失败');
            }
        })
    }
```

### 查询处理

```javascript
testselect(){
        const model = new bankCardModel();
        model.select('','CardId>80',(err,result)=>{
            //console.log(err,result)
            this.res.end('ok');
        })
    }
```

### 删除处理

```javascript
testdelete(){
        const model = new bankCardModel();
        model.delete('CardId=80',(status)=>{
            if(status){
                this.res.end('删除成功');
            }else{
                this.res.end('删除失败');
            }            
        })
    }
```

### 更新处理

```javascript
testupdate(){
        const model = new bankCardModel();
        let data = {
            CardMoney:100,
            CardState:1
        };
        model.update(data,'CardId=89',(status)=>{
            if(status){
                this.res.end('编辑成功');
            }else{
                this.res.end('编辑失败');
            }            
        })
    }
```



## model文件处理

新建一个model.js

来实现简化数据库操作

```javascript
const config = require('../config/config');
const mysqlDb = require('./mysqlDb')

class model{
    //初始化时先判断是那种数据库（如mysql，SQLserver）
    constructor(){
        if(config.db_engin_type == 'mysql'){
            this.dbObj = new mysqlDb();
        }
    }
    //查询
    select(fields,where,callBack){
        //select 字段 from 表名 where 条件
        //如果查询数据是空的，则查询全部数据
        if(fields==''){
            fields = '*'
        }
        //处理后，重组SQL语句
        let sql = `select ${fields} from ${this.name}`;
        if(where){
            sql+=` where ${where}` ;
        }
        //使用mysqlDb
        this.dbObj.select(sql,callBack);
    }
    //新增
    add(data,callBack){
        //insert into 表名(字段1，字段2...) values(值1，值2,....)
        let fields = ``;
        let values = '';
        //对data进行处理
        for (const key in data) {
            if(fields==''){
                fields = key;
            }else{
                fields +=','+key;
            }
            let value = `'${data[key]}'`;
            if(values==''){
                values = value;
            }else{
                values +=','+value;
            }
        }
        //处理后，重组SQL语句
        let sql = `insert into ${this.name}(${fields}) values(${values})`;
        this.dbObj.add(sql,callBack);
    }
    //更新
    update(data,where,callBack){
        let sets = ``;
        //对data进行处理
        for (const key in data) {
            let item = `${key}='${data[key]}'`;
            if(sets==''){
                sets = item;
            }else{
                sets +=','+ item;
            }
        }
       // console.log(sets);return;
        // update 表名 set 字段1=值1,字段2=值2  where 条件
        //处理后，重组SQL语句
        let sql = `update ${this.name} set ${sets}`;
        if(where){
            sql+=` where ${where}` ;
        }
        this.dbObj.update(sql,callBack);
    }
    //删除
    delete(where,callBack){
        //delete from 表名 where 条件
        //处理后，重组SQL语句
        let sql = `delete from ${this.name}`;
        if(where){
            sql+=` where ${where}` ;
        }
        this.dbObj.delete(sql,callBack);
    }

}
module.exports = model;

```